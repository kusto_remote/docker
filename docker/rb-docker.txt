BackEnd

В начале работы нужно установить:
Docker https://docs.docker.com/get-docker/

1) Переходим в папку external_backend и запускаем 
1.1) docker-compose build, ждем
1.2) docker-compose up -d
1.3) проверяем, что контейнеры запущены docker ps -a
2) Делаем импорт БД. 
2.1) Если Windows, то через DBeaver (тогда нужно предварительно его установить и postgres 11.6 тоже)
2.2) Если через Linux, то import должен сработать так docker-compose exec -it <ID Container Postgres> (можно найти через docker ps -a) psql -U local_admin -d postgres < /PATH TO DUMP/planet.dump
3) Добавить изменения в файл **./enviroments/prod/common/config/serv.env** 
	host=postgres - это название контейнера, в котором находится postgres (нужно для того, чтобы другие контейнеры связывались с ним и работала БД)
   ```ini
   db_dsn=pgsql:host=postgres;port=5432;dbname=planet
   db_username=local_admin
   db_password=111222333
   db_charset=utf8
   db_schema=kusto
   
   db_master_dsn=pgsql:host=postgres;port=5432;dbname=planet
   db_master_username=local_admin
   db_master_password=111222333
   db_master_charset=utf8
   db_master_schema=lisa
   
   backend_host=http://adm.loc:8080
   api_host=http://api.loc:8081
   front_host=http://127.0.0.1:3000
   uploads_dir=uploads
   ```
   меняем все host=localhost на host=postgres
   
4) Применить миграции, находять в весте master, командой docker-compose run --rm php-cli php yii migrate
5) Далее инициализируем проект docker-compose run --rm php-cli php init
6) Далее 
	docker-compose run --rm php-cli php yii multisite/default/update-endpoints
	docker-compose run --rm php-cli php yii multisite/default/generate-hostmap
7) Проверить, что в таблице ***lisa.site*** хост api записан как *api.loc:8081*, а файле **./api/runtime/multisite_hostmap.json**  как http://api.loc:8081. 
Если не прописалось - сделать руками.
8) СКОПИРОВАТЬ содержимое **prod_files.tar**  в директорию **./api/web/uploads*

==================
FrontEnd

1) Перейти в папку external_frontend и запустить npm install (предварительно на компе должен стоять nodejs).
2) Меняем в файле .env.rosbank строку API_URL_ROSBANK=https://api.rosbank.ru на API_URL_ROSBANK=http://api.loc:8081
3) Запускаем фронт npm run dev-rb